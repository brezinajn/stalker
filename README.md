# Stalker

Nejnovější development release [zde](https://github.com/brezinajn/Stalker/raw/master/app/app-release.apk).

Nejnovější beta release [zde](https://github.com/brezinajn/Stalker/releases/latest).
