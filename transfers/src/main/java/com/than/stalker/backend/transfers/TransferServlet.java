/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.than.stalker.backend.transfers;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.util.Transfer;
import com.than.stalker.util.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TransferServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();


        Map<String, Object> auth = new HashMap<>();
        auth.put("uid", "service-worker-transfer");

        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setDatabaseUrl("https://stalkerlarp.firebaseio.com")
                    .setServiceAccount(new FileInputStream("stalkerlarp-73ca1c856fd2.json"))
                    .setDatabaseAuthVariableOverride(auth)
                    .build();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FirebaseApp.initializeApp(options);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Transfer.pendingPath)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot i : dataSnapshot.getChildren()) {
                            FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child(Transfer.getPath(i.getKey()))
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Transfer t = dataSnapshot.getValue(Transfer.class);
                                            handleSender(t);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        resp.setStatus(HttpServletResponse.SC_OK);
    }

    public void handleSender(final Transfer t) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(User.getUserPath(t.getSender()))
                .child("balance")
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        long newBalance;
                        if (mutableData.getValue() == null) {
                            newBalance = 0;
                        } else {
                            newBalance = (long) mutableData.getValue();
                        }
                        newBalance -= t.getAmount();

                        if (newBalance < 0) {
                            return Transaction.abort();
                        }

                        mutableData.setValue(newBalance);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        if (databaseError == null) {
                            if (b) {
                                handleRecipient(t);
                            } else {
                                updateTransfer(t, Transfer.Status.REJECTED);
                            }
                        } else {
                            updateTransfer(t, Transfer.Status.FAILED_NOT_SENT);
                        }
                    }
                });

    }

    public void handleRecipient(final Transfer t) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(User.getUserPath(t.getRecipient()))
                .child("balance")
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        long newBalance = t.getAmount();
                        if (mutableData.getValue() != null) {
                            newBalance += (long) mutableData.getValue();
                        }
                        mutableData.setValue(newBalance);

                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        if (databaseError != null || !b) {
                            updateTransfer(t, Transfer.Status.FAILED_NOT_DELIVERED);
                        } else {
                            updateTransfer(t, Transfer.Status.ACCEPTED);
                        }
                    }
                });
    }

    public void updateTransfer(Transfer t, Transfer.Status status) {
        HashMap<String, Object> fanOutTransfer = new HashMap<>();
        fanOutTransfer.put(t.getPendingPath(), null);
        fanOutTransfer.put(t.getPath() + "status", status.name());

        FirebaseDatabase.getInstance().getReference().updateChildren(fanOutTransfer);
    }
}
