# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/than/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepparameternames

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn com.google.firebase.**
-keep class com.google.firebase.** { *; }
-keep interface com.google.firebase.** { *; }

-dontwarn com.fasterxml.jackson.**
-keep class com.fasterxml.jackson.** { *; }
-keep interface com.fasterxml.jackson.** { *; }


-dontwarn com.firebase.ui.**
-keep class com.firebase.ui.** { *; }
-keep interface com.firebase.ui.** { *; }

-dontwarn android.**
-keep class android.** { *; }
-keep interface android.** { *; }




-dontwarn com.than.stalker.**
-keep class com.than.stalker.** { *; }
-keep interface com.than.stalker.** { *; }