package com.than.stalker.app.User;

import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.app.Map.Location;

public class UserLocation extends Location {

    public UserLocation() {
    }

    public UserLocation(double latitude, double longitude) {
        final User au = ActiveUser.getInstance().getUser();

        this.latitude = latitude;
        this.longitude = longitude;
        this.self = au.getSelf();

        if (au != null) {
            FirebaseDatabase.getInstance()
                    .getReference()
                    .child(au.getMyLocationPath())
                    .setValue(this);
        }
    }

    @Override
    public double getLatitude() {
        return super.getLatitude();
    }

    @Override
    public void setLatitude(double latitude) {
        super.setLatitude(latitude);
    }

    @Override
    public double getLongitude() {
        return super.getLongitude();
    }

    @Override
    public void setLongitude(double longitude) {
        super.setLongitude(longitude);
    }


    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }
}
