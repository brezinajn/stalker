package com.than.stalker.app.Map;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.LocationCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;

public class ActivityGoogleMap extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = "ActivityGoogleMap";
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setIndoorEnabled(false);
        mMap.setTrafficEnabled(false);

        final User au = ActiveUser.getInstance().getUser();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("geofire/user/");
        GeoFire geoFire = new GeoFire(ref);

        geoFire.getLocation(au.getSelf(), new LocationCallback() {
            @Override
            public void onLocationResult(String key, GeoLocation location) {
                LatLng player = new LatLng(location.latitude, location.longitude);

                MarkerOptions markerPlayer = new MarkerOptions()
                        .position(player)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_self))
                        .title(au.getName());

                mMap.addMarker(markerPlayer);
                mMap.moveCamera(CameraUpdateFactory.zoomTo(mMap.getMaxZoomLevel()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(player));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
