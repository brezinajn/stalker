package com.than.stalker.app.Conversation;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.than.stalker.R;

public abstract class Conversation {

    public static class ConversationViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public CardView card;

        public ConversationViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
