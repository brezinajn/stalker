package com.than.stalker.app.Banking;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class FragmentTransfer extends Fragment {

    private static final String TAG = "FragmentTransfer";
    private static final String ARG_TRANSFER_TYPE = "transfer-type";
    private TransferType mTransferType = TransferType.RECEIVED;
    private OnListFragmentInteractionListener mListener;
    private FirebaseRecyclerAdapter mAdapter;

    public FragmentTransfer() {
    }

    @SuppressWarnings("unused")
    public static FragmentTransfer newInstance(TransferType transferType) {
        FragmentTransfer fragment = new FragmentTransfer();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRANSFER_TYPE, transferType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mTransferType = (TransferType) getArguments().getSerializable(ARG_TRANSFER_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        Context context = view.getContext();
        final RecyclerView recyclerView = (RecyclerView) view;

        // Set the adapter
        DatabaseReference transferRef = FirebaseDatabase.getInstance()
                .getReference();

        if (mTransferType == TransferType.RECEIVED) {
            transferRef = transferRef.child(ActiveUser.getInstance().getUser().getTransferReceivedsPath());
        } else {
            transferRef = transferRef.child(ActiveUser.getInstance().getUser().getTransferSentsPath());
        }

        mAdapter = new FirebaseRecyclerAdapter<Relation, Transfer.TransferViewHolder>(
                Relation.class, R.layout.card_transfer, Transfer.TransferViewHolder.class, transferRef.orderByChild("created")) {

            @Override
            public void populateViewHolder(final Transfer.TransferViewHolder transferViewHolder, final Relation transfer, final int position) {

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(Transfer.getPath(transfer.getSelf()))
                        .orderByChild("created")
                        .addValueEventListener(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Transfer transfer = dataSnapshot.getValue(Transfer.class);
                                int amount = 0;
                                String user = null;
                                if (transfer != null) {
                                    amount = transfer.getAmount();

                                    if (mTransferType == TransferType.RECEIVED) {
                                        user = transfer.getSender();
                                    } else {
                                        user = transfer.getRecipient();
                                    }
                                }

                                if (user == null) {
                                    transferViewHolder.spinner.setVisibility(View.VISIBLE);
                                    transferViewHolder.amount.setVisibility(View.GONE);
                                    transferViewHolder.user.setVisibility(View.GONE);
                                    transferViewHolder.status.setVisibility(View.GONE);
                                    transferViewHolder.timestamp.setVisibility(View.GONE);
                                } else {
                                    String amountFormatted = NumberFormat.getIntegerInstance()
                                            .format(amount);
                                    transferViewHolder.amount.setText(amountFormatted);
                                    transferViewHolder.spinner.setVisibility(View.GONE);
                                    transferViewHolder.amount.setVisibility(View.VISIBLE);

                                    transferViewHolder.status.setText(transfer.getStatus());
                                    transferViewHolder.status.setVisibility(View.VISIBLE);

                                    SimpleDateFormat sdf = new SimpleDateFormat("MMM d, H:mm");
                                    transferViewHolder.timestamp.setText(sdf.format(transfer.getCreated()));
                                    transferViewHolder.timestamp.setVisibility(View.VISIBLE);

                                    FirebaseDatabase.getInstance()
                                            .getReference()
                                            .child(User.getUserPath(user) + "name")
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    transferViewHolder.user.setText(dataSnapshot.getValue(String.class));
                                                    transferViewHolder.user.setVisibility(View.VISIBLE);
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }
        };

        LinearLayoutManager ll = new LinearLayoutManager(context);
        ll.setReverseLayout(false);
        recyclerView.setLayoutManager(ll);

        recyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public enum TransferType {RECEIVED, SENT}

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Transfer item);
    }
}
