package com.than.stalker.app.User;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActiveUser extends User {
    private static final ActiveUser ourInstance = new ActiveUser();
    private User user;

    private ActiveUser() {
        this.user = null;
    }

    public static ActiveUser getInstance() {
        return ourInstance;
    }

    public void login() {
        final FirebaseUser fu = FirebaseAuth.getInstance().getCurrentUser();

        if (fu != null) {
            FirebaseDatabase.getInstance()
                    .getReference()
                    .child(User.getUserPath(fu.getUid()))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u = dataSnapshot.getValue(User.class);

                            if (u == null) {
                                if (fu.getDisplayName() != null) {
                                    u = new User(fu.getUid(), fu.getDisplayName());
                                } else {
                                    FirebaseAuth.getInstance().signOut();
                                }
                            }

                            ActiveUser.getInstance().setUser(u);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }
    }

    public User getUser() {

        if (user == null ||
                user.getSelf() == null ||
                !user.getSelf().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            login();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSelf() {
        if (user != null) {
            return getUser().getSelf();
        }
        return null;
    }

    public void clearUser() {
        this.user = null;
    }

    @Override
    public String toString() {
        return "ActiveUser{" +
                "user=" + user.toString() +
                '}';
    }
}
