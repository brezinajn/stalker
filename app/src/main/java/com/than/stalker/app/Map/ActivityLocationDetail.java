package com.than.stalker.app.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.Group.FragmentGroupSelect;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.User.User;

import java.util.ArrayList;

public class ActivityLocationDetail extends AppCompatActivity
        implements FragmentGroupSelect.OnListFragmentInteractionListener {
    private static final String TAG = "ActivityLocationDetail";
    Location l;

    FragmentGroupSelect mGroupFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        String locationKey = intent.getStringExtra("location");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Spinner spinner = (Spinner) findViewById(R.id.location_type);
        spinner.setAdapter(new LocationSpinnerAdapter(this));
        spinner.setEnabled(false);

        final TextView ownerName = (TextView) findViewById(R.id.owner);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(Location.getPath(locationKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        l = dataSnapshot.getValue(Location.class);

                        if (l == null) {
                            finish();
                            return;
                        }

                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(User.getUserPath(l.getOwner()))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User owner = dataSnapshot.getValue(User.class);
                                        ownerName.setText(owner.getName());
                                        ownerName.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                        ArrayList<String> locations = (ArrayList<String>) LocationSpinnerAdapter.getListFromStates();
                        spinner.setSelection(locations.indexOf(l.getType()));

                        TextView name = (TextView) findViewById(R.id.name);
                        TextView description = (TextView) findViewById(R.id.description);

                        name.setEnabled(false);
                        description.setEnabled(false);

                        name.setText(l.getName());
                        description.setText(l.getDescription());

                        mGroupFragment = FragmentGroupSelect.newInstance(l.getGroups(), true);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.userListLayout, mGroupFragment)
                                .commit();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        switch (id) {
            case (R.id.save):
                if (l != null) {
                    Location.Types type = Location.Types.valueOf(l.getType());
                    l.edit(l.getName(), l.getDescription(), type, mGroupFragment.getmSelectedGroups());
                } else {
                    LinearLayout ll = (LinearLayout) findViewById(R.id.parent);
                    Snackbar.make(ll, "Error, not saved.", Snackbar.LENGTH_LONG).show();
                }
                finish();
                break;

            case (android.R.id.home):
                onBackPressed();
                break;
            default:
                super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    @Override
    public void onListFragmentInteraction(Group item) {

    }
}
