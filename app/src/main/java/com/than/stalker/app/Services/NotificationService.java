package com.than.stalker.app.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.app.Conversation.Message;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.News.News;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.GroupNotification;
import com.than.stalker.app.Util.MessageNotification;
import com.than.stalker.app.Util.NewsNotification;
import com.than.stalker.app.Util.Relation;
import com.than.stalker.app.Util.TransferNotification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class NotificationService extends Service {
    private static final String TAG = "NotificationService";
    final User au = ActiveUser.getInstance().getUser();
    HashMap<String, Long> lastSeenMessages = new HashMap<>();
    long lastSeenGroup;
    long lastSeenTransfer;
    long lastSeenNews;

    public NotificationService() {
    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {

        if (au == null) {
            this.stopService(intent);
            return startId;
        }
        final Context ctx = this;

        loadPersistentData();


        listenForMessages(ctx);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(News.getPath())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        News n = dataSnapshot.getValue(News.class);

                        if (lastSeenNews < n.getCreated()) {
                            NewsNotification.notify(ctx, n.getTitle(), n.getContent());
                            lastSeenNews = n.getCreated();
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getTransferReceivedsPath())
                .orderByChild("created")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Relation r = dataSnapshot.getValue(Relation.class);

                        if (lastSeenTransfer < r.getCreated()) {
                            TransferNotification.notify(ctx, "New transfer", "You have new pending transfer");
                            lastSeenTransfer = r.getCreated();
                        }


                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupMembersPath())
                .orderByChild("created")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Relation r = dataSnapshot.getValue(Relation.class);

                        if (lastSeenGroup < r.getCreated()) {
                            GroupNotification.notify(ctx, "New group", "You have been invited to a new group");
                            lastSeenGroup = r.getCreated();
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        return super.onStartCommand(intent, flags, startId);
    }

    private void loadPersistentData() {
        try {
            File file = new File(getDir("data", MODE_PRIVATE), au.getSelf() + "lastSeen");
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            lastSeenMessages = (HashMap<String, Long>) ois.readObject();
            lastSeenGroup = ois.readLong();
            lastSeenTransfer = ois.readLong();
            lastSeenNews = ois.readLong();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void listenForMessages(final Context ctx) {
        final User au = ActiveUser.getInstance().getUser();
        if (au == null) {
            return;
        }

        final ValueEventListener vel = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    final Relation r = dataSnapshot.child("lastMessage").getValue(Relation.class);
                    final String group = dataSnapshot.getKey();

                    if (!lastSeenMessages.containsKey(group) || lastSeenMessages.get(group) < r.getCreated()) {
                        lastSeenMessages.remove(group);
                        lastSeenMessages.put(group, r.getCreated());

                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(Message.getMessagePath(group, r.getSelf()))
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Log.d(TAG, "onDataChange: " + dataSnapshot);
                                        Message m = dataSnapshot.getValue(Message.class);
                                        if (m != null && !m.getSender().equals(au.getSelf())) {
                                            MessageNotification.notify(ctx, "New message", m.getText());
                                            FirebaseDatabase.getInstance()
                                                    .getReference()
                                                    .child(Message.getMessagePath(group, r.getSelf()))
                                                    .removeEventListener(this);
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupNotifysPath())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(Group.getPathUtil(dataSnapshot.getKey()))
                                .addValueEventListener(vel);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(Group.getPathLastMessage(dataSnapshot.getKey()))
                                .removeEventListener(vel);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        savePersistentData();
        super.onDestroy();
    }

    private void savePersistentData() {
        if (au == null) {
            return;
        }
        try {
            File file = new File(getDir("data", MODE_PRIVATE), au.getSelf() + "lastSeen");

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(lastSeenMessages);
            oos.writeLong(lastSeenGroup);
            oos.writeLong(lastSeenTransfer);
            oos.writeLong(lastSeenNews);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
