package com.than.stalker.app.User;

import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.Group.Group;

public class FragmentUserPartial extends AUserFragment {
    private String groupKey;

    public FragmentUserPartial() {
    }

    @SuppressWarnings("unused")
    public static FragmentUserPartial newInstance(String groupKey) {
        FragmentUserPartial fragment = new FragmentUserPartial();
        Bundle b = new Bundle();
        b.putString("groupKey", groupKey);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    protected void getArgs() {
        Bundle b = getArguments();
        if (b != null) {
            groupKey = b.getString("groupKey");
        }
    }

    @Override
    protected FirebaseRecyclerAdapter populateAdapter() {
        Query memberRef = FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPath(groupKey))
                .child("members")
                .orderByChild("created");
        return new FirebaseRecyclerAdapter<String, User.UserViewHolder>(
                String.class, R.layout.card_user, User.UserViewHolder.class, memberRef) {
            @Override
            public void populateViewHolder(final User.UserViewHolder userViewHolder, final String user, int position) {
                userViewHolder.spinner.setVisibility(View.VISIBLE);
                userViewHolder.name.setVisibility(View.GONE);
                userViewHolder.selected.setVisibility(View.GONE);

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(User.getUserPath(user))
                        .child("name")
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.getValue(String.class);
                                if (name != null) {
                                    userViewHolder.name.setText(name);
                                    userViewHolder.spinner.setVisibility(View.GONE);
                                    userViewHolder.name.setVisibility(View.VISIBLE);
                                    userViewHolder.selected.setVisibility(View.VISIBLE);
                                    userViewHolder.selected.setChecked(true);
                                    userViewHolder.selected.setEnabled(false);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }
        };
    }
}
