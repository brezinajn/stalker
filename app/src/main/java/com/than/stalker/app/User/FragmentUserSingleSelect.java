package com.than.stalker.app.User;

import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;

public class FragmentUserSingleSelect extends AUserFragment {
    private User mSelectedUser = new User();

    public FragmentUserSingleSelect() {
    }

    @Override
    protected void getArgs() {

    }

    @Override
    protected FirebaseRecyclerAdapter populateAdapter() {
        final User au = ActiveUser.getInstance().getUser();
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(User.path);
        return new FirebaseRecyclerAdapter<User, User.UserViewHolderSingleSelect>(
                User.class, R.layout.card_user_single_select, User.UserViewHolderSingleSelect.class, userRef.orderByChild("sortBy")) {
            @Override
            public void populateViewHolder(final User.UserViewHolderSingleSelect userViewHolder, final User user, int position) {

                String name = user.getName();

                boolean equals = false;
                if (user.getSelf().equals(mSelectedUser.getSelf())) {
                    equals = true;
                }

                if (name == null) {
                    userViewHolder.spinner.setVisibility(View.VISIBLE);
                    userViewHolder.name.setVisibility(View.GONE);
                    userViewHolder.selected.setVisibility(View.GONE);
                } else {
                    userViewHolder.name.setText(name);
                    userViewHolder.spinner.setVisibility(View.GONE);
                    userViewHolder.name.setVisibility(View.VISIBLE);
                    userViewHolder.selected.setVisibility(View.VISIBLE);
                    userViewHolder.selected.setChecked(equals);
                }

                if (au.getSelf().equals(user.getSelf())) {
                    userViewHolder.selected.setEnabled(false);
                } else {
                    userViewHolder.selected.setEnabled(true);

                    userViewHolder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSelectedUser = user;
                            mAdapter.notifyDataSetChanged();
                        }
                    });

                }
            }
        };
    }

    public User getmSelectedUser() {
        return mSelectedUser;
    }
}
