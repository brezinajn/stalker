package com.than.stalker.app.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Location extends com.than.stalker.util.Location {


    public Location() {
    }

    public Location(double x, double y, Location.Types type, String name, String description, HashMap<String, String> groups) {
        this.latitude = x;
        this.longitude = y;
        this.self = FirebaseDatabase.getInstance()
                .getReference()
                .child(Location.path)
                .push()
                .getKey();

        edit(name, description, type, groups, true);
    }

    @Override
    public HashMap<String, String> getGroups() {
        return super.getGroups();
    }

    @Override
    public void setGroups(HashMap<String, String> groups) {
        super.setGroups(groups);
    }

    public void edit(String name, String description, Location.Types type, final HashMap<String, String> grs) {
        edit(name, description, type, grs, false);
    }

    private void edit(String name, String description, Location.Types type, final HashMap<String, String> grs, boolean isNew) {
        final User au = ActiveUser.getInstance().getUser();

        this.name = name;
        this.description = description;
        this.type = type.name();
        if (this.owner == null) {
            this.owner = au.getSelf();
        }


        Map<String, Object> fanOutLocation = new HashMap<>();

        if (isNew) {
            fanOutLocation.put(au.getLocationPath(this.self), this.self);
        }

        if (this.groups == null) {
            this.groups = new HashMap<>();
        }

        HashSet<String> toIgnore = new HashSet<>();
        if (grs != null) {
            for (String group : this.groups.keySet()) {
                if (!grs.containsKey(group)) {
                    // Delete old
                    fanOutLocation.put(Group.getPathLocation(group, this.self), null);
                } else {
                    // Ignore existing
                    toIgnore.add(group);
                }
            }
            this.groups = grs;

            for (String group : this.groups.keySet()) {
                if (!toIgnore.contains(group)) {
                    // Add new
                    fanOutLocation.put(Group.getPathLocation(group, this.self), this.self);
                }
            }
        }

        Object location = new ObjectMapper().convertValue(this, Map.class);
        fanOutLocation.put(this.getPath(), location);

        FirebaseDatabase.getInstance()
                .getReference()
                .updateChildren(fanOutLocation);
    }

    public void delete() {
        HashMap<String, Object> fanOutLocation = new HashMap<>();

        final User au = ActiveUser.getInstance().getUser();
        fanOutLocation.put(this.getPath(), null);
        fanOutLocation.put(au.getLocationPath(this.self), null);

        if (groups != null) {
            for (String group : this.groups.keySet()) {
                fanOutLocation.put(Group.getPathLocation(group, this.self), null);
            }
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .updateChildren(fanOutLocation);
    }

    @Override
    public double getLatitude() {
        return super.getLatitude();
    }

    @Override
    public void setLatitude(double latitude) {
        super.setLatitude(latitude);
    }

    @Override
    public double getLongitude() {
        return super.getLongitude();
    }

    @Override
    public void setLongitude(double longitude) {
        super.setLongitude(longitude);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    @Override
    public String getType() {
        return super.getType();
    }

    @Override
    public void setType(String type) {
        super.setType(type);
    }

    @Override
    public String getOwner() {
        return super.getOwner();
    }

    @Override
    public void setOwner(String owner) {
        super.setOwner(owner);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public void setDescription(String description) {
        super.setDescription(description);
    }
}
