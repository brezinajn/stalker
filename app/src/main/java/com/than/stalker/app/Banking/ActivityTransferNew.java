package com.than.stalker.app.Banking;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.FragmentUserSingleSelect;
import com.than.stalker.app.User.User;

public class ActivityTransferNew extends AppCompatActivity
        implements FragmentUserSingleSelect.OnListFragmentInteractionListener {
    private static final String TAG = "ActivityTransferNew";

    FragmentUserSingleSelect mUserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUserFragment = new FragmentUserSingleSelect();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.userListLayout, mUserFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        switch (id) {
            case (R.id.save):
                User selectedUser = this.mUserFragment.getmSelectedUser();
                TextView amountInput = (TextView) findViewById(R.id.amount);
                int amount;

                try {
                    amount = Integer.parseInt(amountInput.getText().toString());
                } catch (NumberFormatException | NullPointerException e) {
                    amount = 0;
                }


                String errorMessage = null;
                if (amount > 0) {
                    if (selectedUser != null && selectedUser.getSelf() != null) {
                        if (amount <= ActiveUser.getInstance().getUser().getBalance()) {
                            new Transfer(selectedUser.getSelf(), amount);
                        } else {
                            errorMessage = getString(R.string.insufficient_balance);
                        }
                    } else {
                        errorMessage = getString(R.string.no_counterparty);
                    }
                } else {

                    errorMessage = getString(R.string.invalid_amount);
                }

                if (errorMessage == null) {
                    finish();
                } else {
                    Snackbar.make(findViewById(R.id.content), errorMessage, Snackbar.LENGTH_LONG).show();
                }

                break;
            case (android.R.id.home):
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }


    @Override
    public void onListFragmentInteraction(User user) {

    }
}
