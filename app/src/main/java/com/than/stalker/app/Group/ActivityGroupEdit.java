package com.than.stalker.app.Group;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.FragmentUser;
import com.than.stalker.app.User.User;

public class ActivityGroupEdit extends AppCompatActivity implements FragmentUser.OnListFragmentInteractionListener {
    private static final String TAG = "ActivityGroupEdit";
    private Group mGroup;
    private FragmentUser mFragmentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final User au = ActiveUser.getInstance().getUser();

        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        final Bundle b = getIntent().getExtras();
        final String groupKey = b.getString("group");


        final CheckBox shareLocation = (CheckBox) findViewById(R.id.shareLocation);
        final CheckBox notification = (CheckBox) findViewById(R.id.notification);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupNotifyPath(groupKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            notification.setChecked(true);
                        } else {
                            notification.setChecked(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPathSharing(groupKey))
                .child(au.getSelf())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            shareLocation.setChecked(true);
                        } else {
                            shareLocation.setChecked(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        final TextView groupName = (TextView) findViewById(R.id.groupName);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPath(groupKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mGroup = dataSnapshot.getValue(Group.class);
                        if (mGroup != null) {
                            groupName.setText(mGroup.getName());
                            mFragmentUser = FragmentUser.newInstance(mGroup.getMembers());
                            ft.replace(R.id.userListLayout, mFragmentUser).commit();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        finish();

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case (R.id.save):
                TextView groupName = (TextView) findViewById(R.id.groupName);
                CheckBox shareLocation = (CheckBox) findViewById(R.id.shareLocation);
                CheckBox notification = (CheckBox) findViewById(R.id.notification);
                String name = groupName.getText().toString();
                if (!name.isEmpty()) {
                    mGroup.edit(
                            name,
                            mFragmentUser.getmSelectedUsers(),
                            shareLocation.isChecked(),
                            notification.isChecked()
                    );
                    finish();
                } else {
                    Snackbar.make(findViewById(R.id.parent), "Please insert group name", Snackbar.LENGTH_LONG);
                }
                break;
            case (R.id.delete):
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.delete_group_dialog))
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mGroup.delete();
                                finish();
                            }
                        })
                        .setNegativeButton("No", null);
                builder.create().show();
                break;
            case (android.R.id.home):
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }


    @Override
    public void onListFragmentInteraction(User user) {

    }
}
