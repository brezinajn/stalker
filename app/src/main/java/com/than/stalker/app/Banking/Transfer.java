package com.than.stalker.app.Banking;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Transfer extends com.than.stalker.util.Transfer {
    private static final String TAG = "Transfer";

    public Transfer() {
    }

    public Transfer(String recipient, int amount) {
        final User au = ActiveUser.getInstance().getUser();

        this.created = new Date().getTime();
        this.recipient = recipient;
        this.amount = amount;
        this.sender = au.getSelf();
        this.status = Status.PENDING.name();
        this.self = FirebaseDatabase.getInstance()
                .getReference()
                .child(Transfer.path)
                .push()
                .getKey();

        Object transfer = new ObjectMapper().convertValue(this, Map.class);
        Map<String, Object> transferFan = new HashMap<>();
        transferFan.put(this.getPath(), transfer);
        transferFan.put(this.getPendingPath(), this.self);
        transferFan.put(User.getTransferSentPath(au.getSelf(), this.self), Relation.relationFactory(this.self));
        transferFan.put(User.getTransferReceivedPath(recipient, this.self), Relation.relationFactory(this.self));

        FirebaseDatabase.getInstance().getReference().updateChildren(transferFan);
    }

    @Override
    public String getRecipient() {
        return super.getRecipient();
    }

    @Override
    public void setRecipient(String recipient) {
        super.setRecipient(recipient);
    }

    @Override
    @JsonIgnore
    public String getPendingPath() {
        return super.getPendingPath();
    }

    @JsonIgnore
    @Override
    public String getPath() {
        return super.getPath();
    }

    @Override
    public String getSender() {
        return super.getSender();
    }

    @Override
    public void setSender(String sender) {
        super.setSender(sender);
    }

    @Override
    public int getAmount() {
        return super.getAmount();
    }

    @Override
    public void setAmount(int amount) {
        super.setAmount(amount);
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    @Override
    public String getStatus() {
        return super.getStatus();
    }

    @Override
    public void setStatus(String status) {
        super.setStatus(status);
    }

    public static class TransferViewHolder extends RecyclerView.ViewHolder {
        public TextView user;
        public ProgressBar spinner;
        public TextView amount;
        public CardView card;
        public TextView status;
        public TextView timestamp;


        public TransferViewHolder(View itemView) {
            super(itemView);
            user = (TextView) itemView.findViewById(R.id.user);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            amount = (TextView) itemView.findViewById(R.id.amount);
            card = (CardView) itemView.findViewById(R.id.card);
            status = (TextView) itemView.findViewById(R.id.status);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
        }
    }
}
