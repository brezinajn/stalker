package com.than.stalker.app.Group;

import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.util.HashMap;
import java.util.Map;

public class FragmentGroupSelect extends AGroupFragment {
    private static final String TAG = "FragmentGroupSelect";
    private final HashMap<String, String> mSelectedGroups = new HashMap<>();
    private boolean mDisableUnchecked = false;

    public FragmentGroupSelect() {
    }

    public static FragmentGroupSelect newInstance(Map<String, String> groups) {
        return newInstance(groups, false);
    }

    public static FragmentGroupSelect newInstance(Map<String, String> groups, boolean disableUnchecked) {
        if (groups == null) {
            groups = new HashMap<>();
        }

        FragmentGroupSelect fragment = new FragmentGroupSelect();

        Bundle b = new Bundle();
        b.putSerializable("groups", (HashMap<String, String>) groups);
        b.putBoolean("disableUnchecked", disableUnchecked);
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void getArgs() {
        if (getArguments() != null) {
            mSelectedGroups.putAll((HashMap<String, String>) getArguments().getSerializable("groups"));
            mDisableUnchecked = getArguments().getBoolean("disableUnchecked");
        }
    }

    public HashMap<String, String> getmSelectedGroups() {
        return mSelectedGroups;
    }

    @Override
    public FirebaseRecyclerAdapter populateAdapter() {

        final User au = ActiveUser.getInstance().getUser();

        String path;
        if (mDisableUnchecked) {
            path = au.getGroupOwnersPath();
        } else {
            path = au.getGroupAllsPath();
        }

        DatabaseReference groupRef = FirebaseDatabase.getInstance()
                .getReference()
                .child(path);

        return new FirebaseRecyclerAdapter<Relation, Group.GroupViewHolderSelect>(
                Relation.class, R.layout.card_group_select, Group.GroupViewHolderSelect.class, groupRef.orderByChild("created")) {

            @Override
            public void populateViewHolder(final Group.GroupViewHolderSelect groupViewHolder, final Relation group, int position) {


                groupViewHolder.spinner.setVisibility(View.VISIBLE);
                groupViewHolder.select.setVisibility(View.GONE);
                groupViewHolder.titleText.setVisibility(View.GONE);

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(Group.getPath(group.getSelf()))
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final Group g = dataSnapshot.getValue(Group.class);

                                if (g == null || g.getName() == null) {
                                    groupViewHolder.spinner.setVisibility(View.VISIBLE);
                                    groupViewHolder.select.setVisibility(View.GONE);
                                    groupViewHolder.titleText.setVisibility(View.GONE);
                                } else {

                                    boolean contains = mSelectedGroups.containsKey(g.getSelf());

                                    groupViewHolder.titleText.setText(g.getName());
                                    groupViewHolder.select.setChecked(contains);

                                    if (mDisableUnchecked && !contains) {
                                        groupViewHolder.select.setEnabled(false);
                                    } else {
                                        groupViewHolder.card.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                boolean contains = mSelectedGroups.containsKey(g.getSelf());
                                                if (contains) {
                                                    mSelectedGroups.remove(g.getSelf());
                                                } else {
                                                    mSelectedGroups.put(g.getSelf(), g.getSelf());
                                                }

                                                groupViewHolder.select.setChecked(!contains);
                                            }
                                        });
                                    }

                                    groupViewHolder.spinner.setVisibility(View.GONE);
                                    groupViewHolder.select.setVisibility(View.VISIBLE);
                                    groupViewHolder.titleText.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }
        };
    }
}
