package com.than.stalker.app.Conversation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;

public class ActivityConversation extends AppCompatActivity
        implements FragmentMessage.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();

        final User au = ActiveUser.getInstance().getUser();
        final TextView newMessage = (TextView) findViewById(R.id.messageText);
        final ImageButton ib = (ImageButton) findViewById(R.id.sendMessageButton);
        final String conversation = b.getString("conversation");

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupAllPath(conversation))
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPath(conversation))
                .child("name")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String name = dataSnapshot.getValue(String.class);
                        if (name != null) {
                            getSupportActionBar().setTitle(name);
                        } else {
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        finish();
                    }
                });

        assert ib != null;
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                assert newMessage != null;
                String messageText = newMessage.getText().toString();

                if (!messageText.isEmpty()) {
                    new Message(au, conversation, messageText);
                    newMessage.setText("");
                }
            }
        });

        Fragment messagesFragment = new FragmentMessage();
        messagesFragment.setArguments(b);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.conversationRecyclerView, messagesFragment)
                .commit();
    }

    @Override
    public void onListFragmentInteraction(Message item) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
