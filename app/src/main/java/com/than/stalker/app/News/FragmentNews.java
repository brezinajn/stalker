package com.than.stalker.app.News;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.than.stalker.R;

import java.text.SimpleDateFormat;

public class FragmentNews extends Fragment {

    private OnListFragmentInteractionListener mListener;

    private FirebaseRecyclerAdapter mAdapter;

    public FragmentNews() {
    }

    @SuppressWarnings("unused")
    public static FragmentNews newInstance() {
        return new FragmentNews();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        Query newsRef = FirebaseDatabase.getInstance().getReference().child(News.path).orderByPriority();

        // Set the adapter
        final RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        mAdapter = new FirebaseRecyclerAdapter<News, News.NewsViewHolder>(News.class, R.layout.card_news, News.NewsViewHolder.class, newsRef) {
            @Override
            public void populateViewHolder(News.NewsViewHolder newsViewHolder, News news, int position) {
                newsViewHolder.titleText.setText(news.getTitle());
                newsViewHolder.contentText.setText(news.getContent());

                SimpleDateFormat sdf = new SimpleDateFormat("MMM d, H:mm");
                newsViewHolder.timestamp.setText(sdf.format(news.getCreated()));
            }
        };
        recyclerView.setAdapter(mAdapter);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(News item);
    }
}
