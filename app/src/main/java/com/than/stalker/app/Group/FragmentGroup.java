package com.than.stalker.app.Group;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.Util.Relation;

public class FragmentGroup extends AGroupFragment {

    private static final String OWNED = "OWNED";
    private boolean mOwned;

    public FragmentGroup() {
    }

    @SuppressWarnings("unused")
    public static FragmentGroup newInstance(boolean owned) {
        FragmentGroup fragment = new FragmentGroup();
        Bundle args = new Bundle();
        args.putBoolean(OWNED, owned);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void getArgs() {
        mOwned = getArguments().getBoolean(OWNED);
    }

    @Override
    public FirebaseRecyclerAdapter populateAdapter() {
        // Set the adapter
        DatabaseReference groupRef = FirebaseDatabase.getInstance().getReference();
        Drawable d;
        if (mOwned) {
            d = ContextCompat.getDrawable(getActivity(), R.drawable.ic_create_black_24dp);
            groupRef = groupRef.child(ActiveUser.getInstance().getUser().getGroupOwnersPath());
        } else {
            d = ContextCompat.getDrawable(getActivity(), R.drawable.ic_information_outline_black_24dp);
            groupRef = groupRef.child(ActiveUser.getInstance().getUser().getGroupMembersPath());
        }

        final Drawable icon = d;
        return new FirebaseRecyclerAdapter<Relation, Group.GroupViewHolder>(
                Relation.class, R.layout.card_group, Group.GroupViewHolder.class, groupRef.orderByChild("created")) {

            @Override
            public void populateViewHolder(final Group.GroupViewHolder groupViewHolder, final Relation group, int position) {


                groupViewHolder.spinner.setVisibility(View.VISIBLE);
                groupViewHolder.icon.setVisibility(View.GONE);
                groupViewHolder.titleText.setVisibility(View.GONE);

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(Group.getPath(group.getSelf()))
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final Group g = dataSnapshot.getValue(Group.class);

                                if (g == null || g.getName() == null) {
                                    groupViewHolder.spinner.setVisibility(View.VISIBLE);
                                    groupViewHolder.icon.setVisibility(View.GONE);
                                    groupViewHolder.titleText.setVisibility(View.GONE);
                                } else {
                                    groupViewHolder.titleText.setText(g.getName());
                                    groupViewHolder.icon.setImageDrawable(icon);
                                    groupViewHolder.card.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            final String groupId = g.getSelf();
                                            assert groupId != null;

                                            Class c;
                                            if (mOwned) {
                                                c = ActivityGroupEdit.class;
                                            } else {
                                                c = ActivityGroupDetail.class;
                                            }
                                            Intent intent = new Intent(getActivity(), c);

                                            intent.putExtra("group", groupId);
                                            startActivity(intent);
                                        }
                                    });
                                    groupViewHolder.spinner.setVisibility(View.GONE);
                                    groupViewHolder.icon.setVisibility(View.VISIBLE);
                                    groupViewHolder.titleText.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }
        };
    }

}
