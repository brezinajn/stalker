package com.than.stalker.app;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.Banking.ActivityBanking;
import com.than.stalker.app.Conversation.FragmentConversation;
import com.than.stalker.app.Group.ActivityGroup;
import com.than.stalker.app.Map.ActivityGoogleMap;
import com.than.stalker.app.News.FragmentNews;
import com.than.stalker.app.News.News;
import com.than.stalker.app.Services.FirebaseMessagingService;
import com.than.stalker.app.Services.LocationService;
import com.than.stalker.app.Services.NotificationService;
import com.than.stalker.app.Static.Article;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FragmentNews.OnListFragmentInteractionListener,
        FragmentConversation.OnListFragmentInteractionListener {

    public static final int RC_SIGN_IN = 1;
    private static final String TAG = "MainActivity";
    private int mSelectedItem;
    private Activity ctx;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
            if (resultCode == Activity.RESULT_OK && fUser != null) {
                final ActiveUser au = ActiveUser.getInstance();
                au.setUser(new User());
                au.getUser().setSelf(fUser.getUid());
                au.login();

                selectItem(R.id.nav_news);
                startService(new Intent(this, LocationService.class));
                startService(new Intent(this, NotificationService.class));
                startService(new Intent(this, FirebaseMessagingService.class));
                Snackbar.make(findViewById(R.id.main_content), R.string.signin_success, Snackbar.LENGTH_LONG).show();
            } else {
                try {
                    stopService(new Intent(this, LocationService.class));
                    stopService(new Intent(this, NotificationService.class));
                    stopService(new Intent(this, FirebaseMessagingService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (ActiveUser.getInstance().getUser() == null) {
                    signIn();
                }
                Snackbar.make(findViewById(R.id.main_content), R.string.signin_fail, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this;

        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        } catch (DatabaseException e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }

        ActiveUser.getInstance().login();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, perm, 1);
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            selectItem(R.id.nav_news);
        }

        Article.setContext(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                mSelectedItem = -1;
                super.onBackPressed();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            stopService(new Intent(this, LocationService.class));
            stopService(new Intent(this, NotificationService.class));
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Snackbar.make(findViewById(R.id.main_content), "Signed out", Snackbar.LENGTH_LONG).show();
                            signIn();
                        }
                    });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        selectItem(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void selectItem(int id) {

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            signIn();
            return;
        } else if (ActiveUser.getInstance().getUser() == null) {
            ActiveUser.getInstance().login();
            startService(new Intent(this, LocationService.class));
            startService(new Intent(this, NotificationService.class));
            startService(new Intent(this, FirebaseMessagingService.class));
        }
        Intent intent = null;
        Fragment fragment = null;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_news) {
            fragment = new FragmentNews();
        } else if (id == R.id.nav_messages) {
            fragment = new FragmentConversation();
        } else if (id == R.id.nav_groups) {
            intent = new Intent(this, ActivityGroup.class);
        } else if (id == R.id.nav_map) {
            intent = new Intent(this, ActivityGoogleMap.class);
        } else if (id == R.id.nav_banking) {
            intent = new Intent(this, ActivityBanking.class);
        }


        if (fragment != null) {
            if (id != mSelectedItem) {
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.main_content, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        } else if (intent != null) {
            startActivity(intent);
        }
        mSelectedItem = id;
    }

    @Override
    public void onListFragmentInteraction(News item) {

    }

    @Override
    public void onListFragmentInteraction(Relation conversation) {
    }

    private void signIn() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setProviders(
                                AuthUI.EMAIL_PROVIDER)
                        .setTheme(R.style.AppTheme)
                        .build(),
                RC_SIGN_IN);
    }
}
