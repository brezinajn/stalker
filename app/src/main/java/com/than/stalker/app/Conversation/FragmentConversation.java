package com.than.stalker.app.Conversation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.Group.ActivityGroupNew;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.util.ArrayList;

public class FragmentConversation extends Fragment {
    private FirebaseRecyclerAdapter mAdapter;
    private OnListFragmentInteractionListener mListener;
    private ArrayList<ValueEventListener> listeners = new ArrayList<>();

    public FragmentConversation() {
    }

    public static FragmentConversation newInstance() {
        return new FragmentConversation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation_list, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ActivityGroupNew.class));
            }
        });

        // Set the adapter
        final User au = ActiveUser.getInstance().getUser();
        Query conversationRef = FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupAllsPath())
                .orderByChild("created");

        mAdapter = new FirebaseRecyclerAdapter<Relation, Conversation.ConversationViewHolder>(
                Relation.class, R.layout.card_conversation, Conversation.ConversationViewHolder.class, conversationRef) {

            @Override
            public void populateViewHolder(final Conversation.ConversationViewHolder conversationViewHolder, final Relation group, int position) {

                ValueEventListener ves = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String name = dataSnapshot.getValue(String.class);
                        conversationViewHolder.name.setText(name);
                        conversationViewHolder.card.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getContext(), ActivityConversation.class);
                                intent.putExtra("conversation", group.getSelf());
                                startActivity(intent);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(Group.getPath(group.getSelf()))
                        .child("name")
                        .addValueEventListener(ves);
            }
        };

        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Relation item);
    }
}
