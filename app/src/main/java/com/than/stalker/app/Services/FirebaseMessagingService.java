package com.than.stalker.app.Services;
import com.google.firebase.messaging.RemoteMessage;
import com.than.stalker.app.Util.NewsNotification;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    public FirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        NewsNotification.notify(this, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
    }


}
