package com.than.stalker.app.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.than.stalker.R;
import com.than.stalker.app.Group.FragmentGroupSelect;
import com.than.stalker.app.Group.Group;

public class ActivityLocationNew extends AppCompatActivity
        implements FragmentGroupSelect.OnListFragmentInteractionListener {

    FragmentGroupSelect mGroupFragment;

    private double x;
    private double y;
    private Location.Types selectedType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        x = intent.getDoubleExtra("x", 0);
        y = intent.getDoubleExtra("y", 0);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGroupFragment = new FragmentGroupSelect();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.userListLayout, mGroupFragment)
                .commit();

        Spinner spinner = (Spinner) findViewById(R.id.location_type);
        spinner.setAdapter(new LocationSpinnerAdapter(this));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = LocationSpinnerAdapter.getListFromStates().get(position);
                selectedType = Location.Types.valueOf(name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save) {
            TextView nameTv = (TextView) findViewById(R.id.name);
            TextView descriptionTv = (TextView) findViewById(R.id.description);

            String name = nameTv.getText().toString();
            String description = descriptionTv.getText().toString();

            new Location(
                    x,
                    y,
                    selectedType,
                    name,
                    description,
                    mGroupFragment.getmSelectedGroups()
            );
            finish();
            return true;

        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Group item) {

    }
}
