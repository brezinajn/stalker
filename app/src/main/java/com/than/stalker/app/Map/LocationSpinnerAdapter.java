package com.than.stalker.app.Map;

import android.content.Context;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class LocationSpinnerAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {

    public LocationSpinnerAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1, getListFromStates());
    }

    public static List<String> getListFromStates() {
        List<String> list = new ArrayList<>();
        for (Location.Types type : Location.Types.values()) {
            list.add(type.name());
        }
        return list;
    }


}
