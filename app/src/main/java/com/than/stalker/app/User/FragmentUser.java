package com.than.stalker.app.User;

import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;

import java.util.HashMap;
import java.util.Map;

public class FragmentUser extends AUserFragment {
    final private HashMap<String, String> mSelectedUsers = new HashMap<>();

    public FragmentUser() {
    }

    @SuppressWarnings("unused")
    public static FragmentUser newInstance(Map<String, String> selectedUsers) {
        FragmentUser fragment = new FragmentUser();
        Bundle b = new Bundle();
        b.putSerializable("selectedUsers", (HashMap<String, String>) selectedUsers);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    protected void getArgs() {
        Bundle b = getArguments();
        try {
            mSelectedUsers.putAll((HashMap<String, String>) b.getSerializable("selectedUsers"));
        } catch (NullPointerException e) {
            e.printStackTrace(); // TODO: 9.5.16 handle
        }
    }

    @Override
    protected FirebaseRecyclerAdapter populateAdapter() {
        final User au = ActiveUser.getInstance().getUser();
        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(User.path);

        return new FirebaseRecyclerAdapter<User, User.UserViewHolder>(
                User.class, R.layout.card_user, User.UserViewHolder.class, userRef.orderByChild("sortBy")) {
            @Override
            public void populateViewHolder(final User.UserViewHolder userViewHolder, User user, int position) {

                final String key = user.getSelf();
                String name = user.getName();
                boolean contains = mSelectedUsers.containsKey(key);
                if (name == null) {
                    userViewHolder.spinner.setVisibility(View.VISIBLE);
                    userViewHolder.name.setVisibility(View.GONE);
                    userViewHolder.selected.setVisibility(View.GONE);
                } else {
                    userViewHolder.name.setText(name);
                    userViewHolder.spinner.setVisibility(View.GONE);
                    userViewHolder.name.setVisibility(View.VISIBLE);
                    userViewHolder.selected.setVisibility(View.VISIBLE);
                    userViewHolder.selected.setChecked(contains);
                }

                if (au.getSelf().equals(user.getSelf())) {
                    userViewHolder.selected.setEnabled(false);
                } else {
                    userViewHolder.selected.setEnabled(true);
                    userViewHolder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean contains = mSelectedUsers.containsKey(key);
                            userViewHolder.selected.setChecked(!contains);

                            if (contains) {
                                mSelectedUsers.remove(key);
                            } else {
                                mSelectedUsers.put(key, key);
                            }
                        }
                    });

                }
            }
        };
    }

    public HashMap<String, String> getmSelectedUsers() {
        return mSelectedUsers;
    }
}
