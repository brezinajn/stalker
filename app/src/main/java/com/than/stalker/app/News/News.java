package com.than.stalker.app.News;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.than.stalker.R;

public class News extends com.than.stalker.util.News {

    public News() {
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
    }

    @Override
    public String getContent() {
        return super.getContent();
    }

    @Override
    public void setContent(String content) {
        super.setContent(content);
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        public TextView titleText;
        public TextView contentText;
        public TextView timestamp;

        public NewsViewHolder(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.title);
            contentText = (TextView) itemView.findViewById(R.id.content);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
        }
    }
}
