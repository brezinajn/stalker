package com.than.stalker.app.Group;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;

import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.FragmentUser;
import com.than.stalker.app.User.User;

public class ActivityGroupNew extends AppCompatActivity
        implements FragmentUser.OnListFragmentInteractionListener {

    private FragmentUser mFragmentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFragmentUser = new FragmentUser();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.userListLayout, mFragmentUser)
                .commit();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save) {
            TextView groupName = (TextView) findViewById(R.id.groupName);
            String name = groupName.getText().toString();
            CheckBox shareLocation = (CheckBox) findViewById(R.id.shareLocation);
            CheckBox notifications = (CheckBox) findViewById((R.id.notification));
            if (!name.isEmpty()) {
                final User au = ActiveUser.getInstance().getUser();
                au.addGroup(
                        name,
                        mFragmentUser.getmSelectedUsers(),
                        shareLocation.isChecked(),
                        notifications.isChecked()
                );
                finish();
            } else {
                Snackbar.make(
                        findViewById(R.id.parent),
                        R.string.set_a_name,
                        Snackbar.LENGTH_LONG
                ).show();
            }
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(User user) {

    }
}
