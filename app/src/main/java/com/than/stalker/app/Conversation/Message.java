package com.than.stalker.app.Conversation;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.Group.Group;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.util.HashMap;
import java.util.Map;

public class Message extends com.than.stalker.util.Message {

    public Message() {
    }

    public Message(User sender, String conversation, String text) {
        this.text = text.trim();
        this.sender = sender.getSelf();

        String key = FirebaseDatabase.getInstance()
                .getReference()
                .child(getMessagesPath(conversation))
                .push()
                .getKey();

        Object message = new ObjectMapper().convertValue(this, Map.class);
        Object relation = new ObjectMapper().convertValue(new Relation(key), Map.class);

        HashMap<String, Object> fanOutMessage = new HashMap<>();

        fanOutMessage.put(getMessagePath(conversation, key), message);
        fanOutMessage.put(Group.getPathLastMessage(conversation), relation);

        FirebaseDatabase.getInstance()
                .getReference()
                .updateChildren(fanOutMessage);
    }

    @Override
    public String getSender() {
        return super.getSender();
    }

    @Override
    public void setSender(String sender) {
        super.setSender(sender);
    }

    @Override
    public String getText() {
        return super.getText();
    }

    @Override
    public void setText(String text) {
        super.setText(text);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public TextView sender;
        public TextView timestamp;
        public CardView card;
        public LinearLayout parent;

        public MessageViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text);
            sender = (TextView) itemView.findViewById(R.id.sender);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            card = (CardView) itemView.findViewById(R.id.card);
            parent = (LinearLayout) itemView.findViewById(R.id.parent);
        }
    }
}
