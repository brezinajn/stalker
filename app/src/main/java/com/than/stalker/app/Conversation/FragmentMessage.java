package com.than.stalker.app.Conversation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;

import java.text.SimpleDateFormat;

public class FragmentMessage extends Fragment {
    private static final String TAG = "FragmentMessage";

    private static final String ARG_CONVERSATION = "conversation";

    private String mConversation = "";
    private FirebaseRecyclerAdapter mAdapter;
    private OnListFragmentInteractionListener mListener;

    public FragmentMessage() {
    }


    @SuppressWarnings("unused")
    public static FragmentMessage newInstance(String conversation) {
        FragmentMessage fragment = new FragmentMessage();
        Bundle args = new Bundle();
        args.putString(ARG_CONVERSATION, conversation);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mConversation = getArguments().getString(ARG_CONVERSATION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        DatabaseReference messageRef = FirebaseDatabase.getInstance()
                .getReference()
                .child(Message.getMessagesPath(mConversation));

        mAdapter = new FirebaseRecyclerAdapter<Message, Message.MessageViewHolder>(
                Message.class, R.layout.card_message, Message.MessageViewHolder.class, messageRef) {

            @Override
            public void populateViewHolder(final Message.MessageViewHolder messageViewHolder, Message message, int position) {
                final User au = ActiveUser.getInstance().getUser();
                int background;
                if (au.getSelf().equals(message.getSender())) {
                    background = ContextCompat.getColor(getActivity(), R.color.cardAltBackground);
                    messageViewHolder.card.setForegroundGravity(Gravity.END | Gravity.RIGHT);
                    messageViewHolder.sender.setText(au.getName());
                } else {
                    background = ContextCompat.getColor(getActivity(), R.color.cardview_light_background);
                    messageViewHolder.card.setForegroundGravity(Gravity.START | Gravity.LEFT);

                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(User.getUserPath(message.getSender()))
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u = dataSnapshot.getValue(User.class);

                            messageViewHolder.sender.setText(u.getName());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            messageViewHolder.sender.setText(R.string.failed_to_load);
                        }
                    });
                }

                messageViewHolder.card.setCardBackgroundColor(background);
                messageViewHolder.text.setText(message.getText());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM d, H:mm");
                messageViewHolder.timestamp.setText(simpleDateFormat.format(message.getCreated()));
            }
        };

        Context context = view.getContext();
        final RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mAdapter);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Message.path)
                .child(mConversation)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getChildrenCount() > 0) {
                            recyclerView.smoothScrollToPosition((int) dataSnapshot.getChildrenCount() - 1);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Message item);
    }
}
