package com.than.stalker.app.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.Group.FragmentGroupSelect;
import com.than.stalker.app.Group.Group;

import java.util.ArrayList;

public class ActivityLocationEdit extends AppCompatActivity
        implements FragmentGroupSelect.OnListFragmentInteractionListener {
    private static final String TAG = "ActivityLocationEdit";
    Location l;
    Location.Types selectedType;

    FragmentGroupSelect mGroupFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        String locationKey = intent.getStringExtra("location");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Spinner spinner = (Spinner) findViewById(R.id.location_type);
        spinner.setAdapter(new LocationSpinnerAdapter(this));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = LocationSpinnerAdapter.getListFromStates().get(position);
                selectedType = Location.Types.valueOf(name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Location.getPath(locationKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        l = dataSnapshot.getValue(Location.class);

                        if (l == null) {
                            finish();
                            return;
                        }

                        ArrayList<String> locations = (ArrayList<String>) LocationSpinnerAdapter.getListFromStates();
                        spinner.setSelection(locations.indexOf(l.getType()));

                        TextView name = (TextView) findViewById(R.id.name);
                        TextView description = (TextView) findViewById(R.id.description);

                        name.setText(l.getName());
                        description.setText(l.getDescription());

                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(l.getPath())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        mGroupFragment = FragmentGroupSelect.newInstance(l.getGroups());
                                        getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.userListLayout, mGroupFragment)
                                                .commit();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        switch (id) {
            case (R.id.save):
                TextView nameTv = (TextView) findViewById(R.id.name);
                TextView descriptionTv = (TextView) findViewById(R.id.description);

                String name = nameTv.getText().toString();
                String description = descriptionTv.getText().toString();

                l.edit(name, description, selectedType, mGroupFragment.getmSelectedGroups());
                break;
            case (R.id.delete):
                l.delete();
                break;

            case (android.R.id.home):
                onBackPressed();
                break;
            default:
                super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    @Override
    public void onListFragmentInteraction(Group item) {

    }
}
