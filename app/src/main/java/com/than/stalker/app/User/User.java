package com.than.stalker.app.User;

import android.location.Location;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.Group.Group;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class User extends com.than.stalker.util.User {
    private static final String TAG = "User";
    public User() {
    }

    public User(String self, String name) {
        this.self = self;
        this.balance = 0;
        this.name = name.trim();
        this.created = new Date().getTime();
        this.sortBy = this.name.toLowerCase();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(User.getUserPath(this.self))
                .setValue(this);
    }

    public Group addGroup(String groupName, Map<String, String> selectedUsers, boolean shareLocation, boolean notify) {
        return new Group(groupName, this.self, (HashMap<String, String>) selectedUsers, shareLocation, notify);
    }

    public void leaveGroup(String group) {
        Map<String, Object> fanOutLeaveGroup = new HashMap<>();

        fanOutLeaveGroup.put(this.getGroupAllsPath(), null);
        fanOutLeaveGroup.put(Group.getGroupMemberPath(group, this.self), null);
        fanOutLeaveGroup.put(this.getGroupMemberPath(group), null);

        FirebaseDatabase.getInstance().getReference().updateChildren(fanOutLeaveGroup);
    }

    public void shareLocationWithGroup(String group, boolean share) {
        Boolean result = true;
        if (!share) {
            result = null;
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPathSharings(group, this.self))
                .setValue(result);
    }

    public void notifications(String group, boolean notify) {
        User au = ActiveUser.getInstance().getUser();
        Boolean result = true;
        if (!notify) {
            result = null;
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupNotifyPath(group))
                .setValue(result);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public String getSortBy() {
        return super.getSortBy();
    }

    @Override
    public void setSortBy(String sortBy) {
        super.setSortBy(sortBy);
    }

    @Override
    public int getBalance() {
        return super.getBalance();
    }

    @Override
    public void setBalance(int balance) {
        super.setBalance(balance);
    }

    public void updateLocation(Location location) {
        Log.d(TAG, "updateLocation: ");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("geofire/user/");
        GeoFire geoFire = new GeoFire(ref);

        geoFire.setLocation(this.self, new GeoLocation(location.getLatitude(), location.getLongitude()));
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public CheckBox selected;
        public ProgressBar spinner;
        public CardView card;

        public UserViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            selected = (CheckBox) itemView.findViewById(R.id.select);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }


    public static class UserViewHolderSingleSelect extends RecyclerView.ViewHolder {
        public TextView name;
        public RadioButton selected;
        public ProgressBar spinner;
        public CardView card;

        public UserViewHolderSingleSelect(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            selected = (RadioButton) itemView.findViewById(R.id.select);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
