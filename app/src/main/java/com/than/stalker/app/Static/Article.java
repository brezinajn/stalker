package com.than.stalker.app.Static;

import android.content.Context;

import java.io.Serializable;

public class Article implements Serializable {
    protected static Context context;
    protected String title;
    protected String content;
    protected int image;
    public Article() {
    }
    public Article(String title, String content) {
        this.title = title;
        this.content = content;
        this.image = 0;
    }

    public Article(String title, String content, int image) {
        this.title = title;
        this.content = content;
        this.image = image;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Article.context = context;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
