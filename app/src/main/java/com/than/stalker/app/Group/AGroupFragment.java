package com.than.stalker.app.Group;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.than.stalker.R;

public abstract class AGroupFragment extends Fragment {
    private static final String TAG = "FragmentGroup";

    private FirebaseRecyclerAdapter mAdapter;
    private OnListFragmentInteractionListener mListener;

    public AGroupFragment() {
    }

    public abstract void getArgs();

    public abstract FirebaseRecyclerAdapter populateAdapter();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        if (getArguments() != null) {
            getArgs();
        }
        mAdapter = populateAdapter();

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Group item);
    }
}
