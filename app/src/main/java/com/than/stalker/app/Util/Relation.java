package com.than.stalker.app.Util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.than.stalker.util.FirebaseUtility;

import java.util.Map;

public class Relation extends FirebaseUtility {

    public Relation() {
    }

    public Relation(String self) {
        this.self = self;
    }

    public static Map relationFactory(String self) {
        Relation r = new Relation(self);

        return new ObjectMapper().convertValue(r, Map.class);
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }
}
