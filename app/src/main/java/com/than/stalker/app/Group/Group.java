package com.than.stalker.app.Group;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;
import com.than.stalker.R;
import com.than.stalker.app.Conversation.Message;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.User;
import com.than.stalker.app.Util.Relation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Group extends com.than.stalker.util.Group {

    public Group() {
    }

    public Group(String name, String owner, HashMap<String, String> members, boolean location, boolean notify) {

        this.owner = owner;
        this.members = new HashMap<>();
        this.self = FirebaseDatabase.getInstance()
                .getReference()
                .child(path)
                .push()
                .getKey();

        this.edit(name, members, location, notify, true);
    }

    public void edit(String name, HashMap<String, String> members, boolean location, boolean notify) {
        edit(name, members, location, notify, false);
    }

    private void edit(final String name, HashMap<String, String> members, final boolean location, final boolean notify, final boolean isNew) {
        final User au = ActiveUser.getInstance().getUser();
        Map<String, Object> groupFanOut = new HashMap<>();

        assert owner != null;
        assert name != null;
        assert this.self != null;

        this.name = name.trim();

        members.remove(this.owner);

        if (this.members == null) {
            this.members = new HashMap<>();
        }

        HashSet<String> toIgnore = new HashSet<>();
        for (String member : this.members.keySet()) {
            if (!members.containsKey(member)) {
                // Delete members
                groupFanOut.put(User.getGroupMemberPath(member, this.self), null);
                groupFanOut.put(User.getGroupAllPath(member, this.self), null);
            } else {
                // Ignore existing members
                toIgnore.add(member);
            }
        }

        this.members = members;
        for (String member : this.members.keySet()) {
            if (!toIgnore.contains(member)) {
                // Add new members
                groupFanOut.put(User.getGroupMemberPath(member, this.self), Relation.relationFactory(this.self));
                groupFanOut.put(User.getGroupAllPath(member, this.self), Relation.relationFactory(this.self));
                this.members.put(member, member);
            }
        }

        Object group = new ObjectMapper().convertValue(this, Map.class);
        groupFanOut.put(getPath(), group);

        // Set owner values
        if (isNew) {
            groupFanOut.put(User.getGroupAllPath(this.owner, this.self), Relation.relationFactory(this.self));
            groupFanOut.put(User.getGroupOwnerPath(this.owner, this.self), Relation.relationFactory(this.self));
        }

        // Owner location
        if (location) {
            groupFanOut.put(this.getPathSharings(au.getSelf()), true);
        } else {
            groupFanOut.put(this.getPathSharings(au.getSelf()), null);
        }

        // Owner notification
        if (notify) {
            groupFanOut.put(au.getGroupNotifyPath(this.self), true);
        } else {
            groupFanOut.put(au.getGroupNotifyPath(this.self), null);
        }

        FirebaseDatabase.getInstance().getReference().updateChildren(groupFanOut);
    }

    public void delete() {
        HashMap<String, Object> fanOutDelete = new HashMap<>();

        fanOutDelete.put(getPath(), null);
        fanOutDelete.put(Message.path + this.self, null);
        fanOutDelete.put(ActiveUser.getInstance().getUser().getGroupOwnerPath(this.self), null);
        fanOutDelete.put(this.getPathUtil(), null);

        fanOutDelete.put(User.getGroupAllPath(owner, this.self), null);
        if (this.members != null) {
            for (String user : this.members.keySet()) {
                fanOutDelete.put(User.getGroupMemberPath(user, this.self), null);
                fanOutDelete.put(User.getGroupAllPath(user, this.self), null);
            }
        }

        FirebaseDatabase.getInstance().getReference().updateChildren(fanOutDelete);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        return this.self.equals(group.getSelf());

    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getOwner() {
        return super.getOwner();
    }

    @Override
    public void setOwner(String owner) {
        super.setOwner(owner);
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    @Override
    public Map<String, String> getMembers() {
        return super.getMembers();
    }

    @Override
    public void setMembers(Map<String, String> members) {
        super.setMembers(members);
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        public TextView titleText;
        public ProgressBar spinner;
        public ImageView icon;
        public CardView card;


        public GroupViewHolder(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.title);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            icon = (ImageView) itemView.findViewById(R.id.imageView);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }


    public static class GroupViewHolderSelect extends RecyclerView.ViewHolder {
        public TextView titleText;
        public ProgressBar spinner;
        public CheckBox select;
        public CardView card;


        public GroupViewHolderSelect(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.title);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            select = (CheckBox) itemView.findViewById(R.id.checkBox);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
