package com.than.stalker.app.Group;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.than.stalker.R;
import com.than.stalker.app.User.ActiveUser;
import com.than.stalker.app.User.FragmentUserPartial;
import com.than.stalker.app.User.User;

public class ActivityGroupDetail extends AppCompatActivity
        implements FragmentUserPartial.OnListFragmentInteractionListener {
    private static final String TAG = "ActivityGroupDetail";

    private Group mGroup;
    private FragmentUserPartial mUserFragment;
    private String groupKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView groupName = (TextView) findViewById(R.id.groupName);
        groupName.setEnabled(false);


        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        final Bundle b = getIntent().getExtras();
        groupKey = b.getString("group");
        User au = ActiveUser.getInstance().getUser();

        final CheckBox shareLocation = (CheckBox) findViewById(R.id.shareLocation);
        final CheckBox notification = (CheckBox) findViewById(R.id.notification);

        FirebaseDatabase.getInstance()
                .getReference()
                .child(au.getGroupNotifyPath(groupKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            notification.setChecked(true);
                        } else {
                            notification.setChecked(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPathSharing(groupKey))
                .child(au.getSelf())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            shareLocation.setChecked(true);
                        } else {
                            shareLocation.setChecked(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        final TextView ownerName = (TextView) findViewById(R.id.owner);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(Group.getPath(groupKey))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mGroup = dataSnapshot.getValue(Group.class);
                        if (mGroup != null) {
                            groupName.setText(mGroup.getName());
                            mUserFragment = FragmentUserPartial.newInstance(mGroup.getSelf());
                            ft.replace(R.id.userListLayout, mUserFragment).commit();

                            FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child(User.getUserPath(mGroup.getOwner()))
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            User owner = dataSnapshot.getValue(User.class);
                                            ownerName.setVisibility(View.VISIBLE);
                                            ownerName.setText(owner.getName());
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        finish();

                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_detail, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case (R.id.leave):
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.leave_group_dialog))
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ActiveUser.getInstance().getUser().leaveGroup(groupKey);
                                finish();
                            }
                        })
                        .setNegativeButton("No", null);
                builder.create().show();
                break;
            case (R.id.save):
                final User au = ActiveUser.getInstance().getUser();
                CheckBox shareLocation = (CheckBox) findViewById(R.id.shareLocation);
                CheckBox notification = (CheckBox) findViewById(R.id.notification);

                au.shareLocationWithGroup(groupKey, shareLocation.isChecked());
                au.notifications(groupKey, notification.isChecked());
                finish();

                break;
            case (R.id.home):
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onListFragmentInteraction(User user) {

    }
}
