package com.than.stalker.utils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class News {
    public static final String path = "news";

    private String title;
    private String content;

    public News() {
    }

    public String getTitle() {
        return title;
    }


    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        public TextView titleText;
        public TextView contentText;

        public NewsViewHolder(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.title);
            contentText = (TextView) itemView.findViewById(R.id.content);
        }
    }
}
