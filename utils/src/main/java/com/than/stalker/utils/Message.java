package com.than.stalker.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class Message {
    public static final String path = "messages/";
    private static final String TAG = "Message";
    private String text;
    private String sender;
    private long created;

    public Message() {
    }

    public Message(User sender, String conversation, String text) {
        this.text = text.trim();
        this.sender = sender.getSelf();
        this.created = new Date().getTime();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(getMessagesPath(conversation))
                .push()
                .setValue(this);

    }

    public static String getMessagesPath(String conversation) {
        return path + conversation + "/";
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSelf() {
        return null;
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public TextView sender;
        public TextView timestamp;
        public CardView card;

        public MessageViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text);
            sender = (TextView) itemView.findViewById(R.id.sender);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
