package com.than.stalker.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Transfer {
    private static final String TAG = "Transfer";


    private static final String path = "/userUtils/transfers/%s/";
    private static final String sentPath = "/userUtils/transfers/%s/sent/";
    private static final String receivedPath = "/userUtils/transfers/%s/received/";

    private String recipient;
    private String sender;
    private int amount;
    private String self;
    private long created;

    public Transfer() {
    }

    public Transfer(String recipient, int amount) {
        User au = ActiveUser.getInstance().getUser();

        this.created = new Date().getTime();
        this.recipient = recipient;
        this.amount = amount;
        this.sender = au.getSelf();
        this.self = FirebaseDatabase.getInstance()
                .getReference()
                .child(getSentPath(au.getSelf()))
                .push()
                .getKey();

        assert this.self != null;
        assert this.sender != null;
        assert this.recipient != null;
        assert this.amount > 0;

        Object transfer = new ObjectMapper().convertValue(this, Map.class);

        Map<String, Object> transferFan = new HashMap<>();
        transferFan.put(getSentPath(au.getSelf()) + this.self, transfer);
        transferFan.put(getReceivedPath(recipient) + this.self, transfer);

        FirebaseDatabase.getInstance().getReference().updateChildren(transferFan);
    }

    public static String getPath(String userId) {
        return String.format(path, userId);
    }

    public static String getSentPath(String userId) {
        return String.format(sentPath, userId);
    }

    public static String getReceivedPath(String userId) {
        return String.format(receivedPath, userId);
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public static class TransferViewHolder extends RecyclerView.ViewHolder {
        public TextView user;
        public ProgressBar spinner;
        public TextView amount;
        public CardView card;


        public TransferViewHolder(View itemView) {
            super(itemView);
            user = (TextView) itemView.findViewById(R.id.user);
            //spinner = (ProgressBar) itemView.findViewById(R.id.progressBar);
            amount = (TextView) itemView.findViewById(R.id.amount);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
