package com.than.stalker.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Group {
    public static final String path = "groups/";
    private static final String TAG = "Group";
    protected String name;
    protected String owner;
    protected Map<String, String> members;
    protected String self;
    protected long created;

    public Group() {
    }

    public Group(String name, String owner, HashMap<String, String> members) {
        DatabaseReference newGroupRef = FirebaseDatabase.getInstance().getReference().child(path).push();

        this.self = newGroupRef.getKey();
        this.owner = owner;
        this.members = new HashMap<>();
        this.created = new Date().getTime();

        this.edit(name, members);
    }

    public static String getGroupMemberPath(String group, String user) {
        return path + group + "/members/" + user + "/";
    }

    public static String getPath(String groupId) {
        return path + groupId + "/";
    }

    public String getGroupMemberPath(String user) {
        return Group.getGroupMemberPath(this.self, user);
    }

    public Map<String, String> getMembers() {
        return members;
    }

    public void setMembers(Map<String, String> members) {
        this.members = members;
    }

    @Exclude
    public String getPath() {
        return path + this.self + "/";
    }

    public void edit(String name, HashMap<String, String> members) {
        Map<String, Object> groupFanOut = new HashMap<>();

        assert owner != null;
        assert name != null;
        assert this.self != null;

        this.name = name.trim();

        members.remove(this.owner);

        if (this.members != null) {
            for (Map.Entry entry : this.members.entrySet()) {
                if (!members.containsKey(entry.getKey().toString())) {
                    groupFanOut.put(User.groupMemberPath + entry.getKey() + "/" + this.self, null); // TODO: 7.5.16 this can reenable the notif.
                    groupFanOut.put(Conversation.path + entry.getKey() + "/" + this.self, null);
                }
            }
            this.members.clear();
        } else {
            this.members = new HashMap<>();
        }

        this.members.putAll(members);
        Object group = new ObjectMapper().convertValue(this, Map.class);

        groupFanOut.put(getPath(), group);
        groupFanOut.put(Conversation.path + this.owner + "/" + this.self, this.self);
        groupFanOut.put(User.getGroupOwnerPath(this.owner, this.self), this.self);

        for (Map.Entry entry : this.members.entrySet()) {
            String member = (String) entry.getKey();
            groupFanOut.put(User.getGroupMemberPath(member, this.self), this.self);
            groupFanOut.put(Conversation.path + member + "/" + this.self, this.self);
        }


        FirebaseDatabase.getInstance().getReference().updateChildren(groupFanOut, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(TAG, "onComplete: " + databaseError);
                }
            }
        });
    }

    public void delete() {
        // TODO: 20.5.16 remove all listeners
        HashMap<String, Object> fanOutDelete = new HashMap<>();

        fanOutDelete.put(path + this.self, null);
        fanOutDelete.put(Message.path + this.self, null);
        fanOutDelete.put(ActiveUser.getInstance().getUser().getGroupOwnerPath(this.self), null);

        fanOutDelete.put(Conversation.path + this.owner + "/" + this.self, null);
        if (this.members != null) {
            for (Map.Entry entry : this.members.entrySet()) {
                fanOutDelete.put(User.getGroupMemberPath(entry.getKey().toString(), this.self), null);
                fanOutDelete.put(Conversation.path + entry.getKey() + "/" + this.self, null);
            }
        }

        FirebaseDatabase.getInstance().getReference().updateChildren(fanOutDelete);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        return self.equals(group.self);

    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        return self.hashCode();
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", members=" + members +
                ", self='" + self + '\'' +
                ", created=" + created +
                '}';
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        public TextView titleText;
        public ProgressBar spinner;
        public ImageView icon;
        public CardView card;


        public GroupViewHolder(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.title);
            spinner = (ProgressBar) itemView.findViewById(R.id.progressBar);
            icon = (ImageView) itemView.findViewById(R.id.imageView);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
