package com.than.stalker.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class Conversation {
    public static String path = "conversations/";

    public Conversation() {
    }


    public static String getConversationPath(String userId, String groupId) {
        return path + userId + "/" + groupId + "/";
    }
    public static class ConversationViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public CardView card;

        public ConversationViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
