package com.than.stalker.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActiveUser {
    private static final String TAG = "ActiveUser";
    private static ActiveUser ourInstance = new ActiveUser();
    private User user;

    private ActiveUser() {
        this.user = null;
    }

    public static ActiveUser getInstance() {
        return ourInstance;
    }

    public boolean login() {
        final FirebaseUser fu = FirebaseAuth.getInstance().getCurrentUser();

        if (fu != null) {
            FirebaseDatabase.getInstance()
                    .getReference()
                    .child(User.getUserPath(fu.getUid()))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u = dataSnapshot.getValue(User.class);

                            if (u == null) {
                                u = new User(fu.getUid(), fu.getDisplayName());
                            }

                            ActiveUser.getInstance().setUser(u);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return true;
        } else {
            return false;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSelf() {
        return user.getSelf();
    }

    public void clearUser() {
        this.user = null;
    }

    @Override
    public String toString() {
        return "ActiveUser{" +
                "user=" + user.toString() +
                '}';
    }
}
