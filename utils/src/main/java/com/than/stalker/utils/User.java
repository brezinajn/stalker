package com.than.stalker.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class User {
    public static final String path = "users/";
    public static final String groupOwnerPath = "userUtils/groups/%s/owners/%s";
    public static final String groupMemberPath = "userUtils/groups/%s/members/%s";
    private static final String TAG = "User";
    protected String name;
    protected String self;
    protected long created;
    protected String sortBy;

    public User() {
    }

    public User(String self, String name) {
        this.self = self;
        this.name = name.trim();
        this.created = new Date().getTime();
        this.sortBy = this.name.toLowerCase();

        FirebaseDatabase.getInstance()
                .getReference()
                .child(User.getUserPath(this.self))
                .setValue(this, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            Log.e(TAG, "onComplete: " + databaseError.getMessage());
                        }
                    }
                });
    }

    public static String getUserPath(String uid) {
        return path + uid + "/";
    }

    @Exclude
    public static String getGroupOwnerPath(String userId, String groupId) {
        return String.format(groupOwnerPath, userId, groupId + "/");
    }

    public static String getGroupMemberPath(String userId, String groupId) {
        return String.format(groupMemberPath, userId, groupId) + "/";
    }

    @Exclude
    public String getGroupOwnerPath(String groupId) {
        return String.format(groupOwnerPath, this.self, groupId + "/");
    }

    public String getGroupOwnerPath() {
        return String.format(groupOwnerPath, this.self, "");
    }

    public String getGroupMemberPath() {
        return String.format(groupMemberPath, this.self, "");
    }

    public String getGroupMemberPath(String groupId) {
        return User.getGroupMemberPath(this.self, groupId);
    }

    public Group addGroup(String groupName, Map<String, String> selectedUsers) {
        return new Group(groupName, this.self, (HashMap<String, String>) selectedUsers);
    }

    public void leaveGroup(String group) {
        Map<String, Object> fanoutLeaveGroup = new HashMap<>();

        fanoutLeaveGroup.put(Conversation.getConversationPath(this.self, group), null);
        fanoutLeaveGroup.put(Group.getGroupMemberPath(group, this.self), null);
        fanoutLeaveGroup.put(this.getGroupMemberPath(group), null);

        FirebaseDatabase.getInstance().getReference().updateChildren(fanoutLeaveGroup);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", self='" + self + '\'' +
                ", created=" + created +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public CheckBox selected;
        public ProgressBar spinner;
        public CardView card;

        public UserViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            selected = (CheckBox) itemView.findViewById(R.id.checkBox);
            spinner = (ProgressBar) itemView.findViewById(R.id.spinner);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
