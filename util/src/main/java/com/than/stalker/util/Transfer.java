package com.than.stalker.util;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Transfer extends FirebaseUtility {
    public static final String path = "transfers/";
    public static final String pendingPath = "transferUtils/pending/";
    private static final String TAG = "Transfer";
    protected String recipient;
    protected String sender;
    protected int amount;
    protected String status;

    public Transfer() {
    }

    public static String getPath(String transferId) {
        return path + transferId + "/";
    }

    public static String getPendingPath(String transferId) {
        return pendingPath + transferId + "/";
    }


    @JsonIgnore
    public String getPath() {
        return getPath(this.self);
    }

    @JsonIgnore
    public String getPendingPath() {
        return getPendingPath(this.self);
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String getSelf() {
        return super.getSelf();
    }

    @Override
    public void setSelf(String self) {
        super.setSelf(self);
    }

    @Override
    public long getCreated() {
        return super.getCreated();
    }

    @Override
    public void setCreated(long created) {
        super.setCreated(created);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public enum Status {
        PENDING, ACCEPTED, REJECTED, FAILED_NOT_SENT, FAILED_NOT_DELIVERED
    }
}
