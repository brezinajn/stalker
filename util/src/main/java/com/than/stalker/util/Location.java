package com.than.stalker.util;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;

public class Location extends FirebaseUtility {

    public static final String path = "locations/";
    protected static final String TAG = "Location";
    protected double latitude;
    protected double longitude;
    protected String name;
    protected String description;
    protected HashMap<String, String> groups;
    protected String type;
    protected String owner;
    public Location() {
    }

    @JsonIgnore
    public static String getPath(String locationId) {
        return path + locationId + "/";
    }

    public HashMap<String, String> getGroups() {
        return groups;
    }

    public void setGroups(HashMap<String, String> groups) {
        this.groups = groups;
    }

    @JsonIgnore
    public String getPath() {
        return getPath(this.self);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Location{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", groups=" + groups +
                ", type='" + type + '\'' +
                ", owner='" + owner + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        return self.equals(location.self);

    }

    @Override
    public int hashCode() {
        return owner.hashCode();
    }

    public enum Types {
        Stash,
        Radiation,
        Danger,
        Anomaly,
        Psy,
        Biohazard,
        Location,
        Haklivost
    }
}

