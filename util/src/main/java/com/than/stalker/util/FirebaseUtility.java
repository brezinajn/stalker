package com.than.stalker.util;

import java.util.Date;

public abstract class FirebaseUtility {
    protected String self;
    protected long created;

    public FirebaseUtility() {
        this.created = new Date().getTime();
    }

    @Override
    public String toString() {
        return "FirebaseUtility{" +
                "self='" + self + '\'' +
                ", created=" + created +
                '}';
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

}
