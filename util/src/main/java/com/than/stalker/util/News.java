package com.than.stalker.util;

public abstract class News extends FirebaseUtility {
    public static final String path = "news/";

    protected String title;
    protected String content;

    public News() {
    }

    public static String getPath() {
        return path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
