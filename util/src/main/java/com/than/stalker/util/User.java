package com.than.stalker.util;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class User extends FirebaseUtility {
    public static final String path = "users/";
    public static final String utilPath = "userUtils/";

    public static final String myPath = utilPath + "user/%s/";
    public static final String myLocationPath = myPath + "location/";

    public static final String groupPath = utilPath + "groups/";
    public static final String groupAllPath = groupPath + "%s/all/";
    public static final String groupOwnerPath = groupPath + "%s/owners/";
    public static final String groupMemberPath = groupPath + "%s/members/";
    public static final String groupNotifyPath = groupPath + "%s/notify/";

    public static final String transferPath = utilPath + "transfers/";
    public static final String transferSentPath = transferPath + "%s/sent/";
    public static final String transferReceivedPath = transferPath + "%s/received/";

    public static final String locationPath = utilPath + "locations/";

    private static final String TAG = "User";
    protected String name;
    protected int balance;
    protected String sortBy;

    @JsonIgnore
    public static String getMyLocationPath(String user) {
        return String.format(myLocationPath, user);
    }

    @JsonIgnore
    public static String getGroupNotifysPath(String user) {
        return String.format(groupNotifyPath, user);
    }

    @JsonIgnore
    public static String getGroupNotifyPath(String user, String group) {
        return getGroupNotifysPath(user) + group + "/";
    }

    @JsonIgnore
    public static String getUserPath(String user) {
        return path + user + "/";
    }

    @JsonIgnore
    public static String getLocationPath(String user, String location) {
        return locationPath + user + "/" + location + "/";
    }

    @JsonIgnore
    public static String getGroupOwnerPath(String user, String group) {
        return String.format(groupOwnerPath, user) + group + "/";
    }

    @JsonIgnore
    public static String getGroupAllPath(String user, String group) {
        return getGroupAllsPath(user) + group + "/";
    }

    @JsonIgnore
    public static String getGroupAllsPath(String user) {
        return String.format(groupAllPath, user);
    }

    @JsonIgnore
    public static String getGroupMemberPath(String user, String group) {
        return String.format(groupMemberPath, user) + group + "/";
    }

    @JsonIgnore
    public static String getTransferSentPath(String user, String transfer) {
        return String.format(transferSentPath, user) + transfer + "/";
    }

    @JsonIgnore
    public static String getTransferReceivedPath(String user, String transfer) {
        return String.format(transferReceivedPath, user) + transfer + "/";
    }

    @JsonIgnore
    public static String getLocationsPath(String user) {
        return locationPath + user + "/";
    }

    @JsonIgnore
    public String getGroupNotifysPath() {
        return getGroupNotifysPath(this.self);
    }

    @JsonIgnore
    public String getGroupNotifyPath(String group) {
        return getGroupNotifyPath(this.self, group);
    }

    @JsonIgnore
    public String getGroupAllPath(String group) {
        return getGroupAllPath(this.self, group);
    }

    @JsonIgnore
    public String getGroupAllsPath() {
        return getGroupAllsPath(this.self);
    }

    @JsonIgnore
    public String getMyLocationPath() {
        return getMyLocationPath(this.self);
    }

    @JsonIgnore
    public String getLocationPath(String location) {
        return getLocationPath(this.self, location);
    }

    @JsonIgnore
    public String getUserPath() {
        return getUserPath(this.self);
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @JsonIgnore
    public String getTransferSentPath(String transfer) {
        return getTransferSentPath(this.self, transfer);
    }

    @JsonIgnore
    public String getTransferReceivedPath(String transfer) {
        return getTransferReceivedPath(this.self, transfer);
    }

    @JsonIgnore
    public String getTransferSentsPath() {
        return String.format(transferSentPath, this.self);
    }

    @JsonIgnore
    public String getTransferReceivedsPath() {
        return String.format(transferReceivedPath, this.self);
    }

    @JsonIgnore
    public String getGroupOwnerPath(String group) {
        return String.format(groupOwnerPath, this.self) + group + "/";
    }

    @JsonIgnore
    public String getGroupOwnersPath() {
        return String.format(groupOwnerPath, this.self);
    }

    @JsonIgnore
    public String getGroupMembersPath() {
        return String.format(groupMemberPath, this.self);
    }

    @JsonIgnore
    public String getGroupMemberPath(String group) {
        return User.getGroupMemberPath(this.self, group);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", self='" + self + '\'' +
                ", created=" + created +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortBy() {
        return name.toLowerCase();
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    @JsonIgnore
    public String getLocationsPath() {
        return getLocationsPath(this.self);
    }
}
