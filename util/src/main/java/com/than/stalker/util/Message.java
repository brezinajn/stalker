package com.than.stalker.util;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Message extends FirebaseUtility {
    public static final String path = "messages/";
    private static final String TAG = "Message";
    protected String text;
    protected String sender;


    @JsonIgnore
    public static String getMessagesPath(String conversation) {
        return path + conversation + "/";
    }

    @JsonIgnore
    public static String getMessagePath(String conversation, String message) {
        return getMessagesPath(conversation) + message + "/";
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
