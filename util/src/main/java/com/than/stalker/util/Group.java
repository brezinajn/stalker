package com.than.stalker.util;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

public abstract class Group extends FirebaseUtility {
    public static final String path = "groups/";
    public static final String pathGroupMembers = path + "%s/members/";

    public static final String pathUtil = "groupUtils/%s/";
    public static final String pathLocations = pathUtil + "locations/";
    public static final String pathSharing = pathUtil + "sharing/";
    public static final String pathLastMessage = pathUtil + "lastMessage/";

    protected String name;
    protected String owner;
    protected Map<String, String> members;

    @JsonIgnore
    public static String getPathLastMessage(String group) {
        return String.format(pathLastMessage, group);
    }

    @JsonIgnore
    public static String getPathUtil(String group) {
        return String.format(pathUtil, group);
    }

    @JsonIgnore
    public static String getPathLocation(String group, String location) {
        return getPathLocations(group) + location + "/";
    }

    @JsonIgnore
    public static String getGroupMemberPath(String group, String user) {
        return String.format(pathGroupMembers, group) + user + "/";
    }

    @JsonIgnore
    public static String getPath(String groupId) {
        return path + groupId + "/";
    }

    @JsonIgnore
    public static String getPathLocations(String group) {
        return String.format(pathLocations, group);
    }

    @JsonIgnore
    public static String getPathSharing(String group) {
        return String.format(pathSharing, group);
    }

    @JsonIgnore
    public static String getPathSharings(String group, String user) {
        return getPathSharing(group) + user + "/";
    }

    @JsonIgnore
    public String getPathLastMessage() {
        return getPathLastMessage(this.self);
    }

    @JsonIgnore
    public String getPathUtil() {
        return getPathUtil(this.self);
    }

    @JsonIgnore
    public String getPathSharing() {
        return getPathSharing(this.self);
    }

    @JsonIgnore
    public String getPathSharings(String user) {
        return getPathSharings(this.self, user);
    }


    @JsonIgnore
    public String getPathLocations() {
        return getPathLocations(this.self);
    }

    @JsonIgnore
    public String getPathLocation(String location) {
        return getPathLocation(this.self, location);
    }

    @JsonIgnore
    public String getGroupMemberPath(String user) {
        return Group.getGroupMemberPath(this.self, user);
    }

    public Map<String, String> getMembers() {
        return members;
    }

    public void setMembers(Map<String, String> members) {
        this.members = members;
    }


    @JsonIgnore
    public String getPath() {
        return path + this.self + "/";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", members=" + members +
                ", self='" + self + '\'' +
                ", created=" + created +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return this.self.equals(group.getSelf());

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (members != null ? members.hashCode() : 0);
        return result;
    }
}
